import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { addNavigationHelpers, StackNavigator } from 'react-navigation';

import Login from '../pages/Login';
import AssetDetail from '../pages/AssetDetail';
import ScanBarcode from '../pages/ScanBarcode';
import Setting from '../pages/Setting';

import { addListener } from '../utils/redux';

export const AppNavigator = StackNavigator({
    Login: { screen: Login },
    AssetDetail: { screen: AssetDetail },
    ScanBarcode: { screen: ScanBarcode },
    Setting: { screen: Setting },
});

class AppWithNavigationState extends React.Component {
  static propTypes = {
    dispatch: PropTypes.func.isRequired,
    nav: PropTypes.object.isRequired,
  };

  render() {
    const { dispatch, nav } = this.props;
    return (
      <AppNavigator
        navigation={addNavigationHelpers({
          dispatch,
          state: nav,
          addListener,
        })}
      />
    );
  }
}

const mapStateToProps = state => ({
  nav: state.nav,
});

export default connect(mapStateToProps)(AppWithNavigationState);

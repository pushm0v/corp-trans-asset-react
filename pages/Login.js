import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import renderIf from 'render-if';
import { ScrollView, Text, View, Button, StyleSheet, TouchableHighlight } from 'react-native';
import { authUserLogin } from '../redux/actions/auth/user-login';
import { getAuthSelector } from '../redux/reducers/auth';
import { FloatingLabelInput } from '../components/FloatingLabelInput';
import { storage, storageKey } from '../utils/storage';
import { config } from '../lib/config';

import Icon from 'react-native-vector-icons/FontAwesome';
import Spinner from 'react-native-loading-spinner-overlay';


const styles = StyleSheet.create({
    errorView: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    },
    errorMessage: {
        textAlign: 'center',
        margin: 3,
        fontWeight: 'bold',
        color: 'red'
    }
});

class Login extends Component {
    constructor (props) {
        super(props);
        this.state = {
            userToken: '',
            username: '',
            password: '',
        };
    }

    componentDidMount() {
        storage.load({
            key: storageKey,
            autoSync: true,
            syncInBackground: true,
        }).then(ret => {
            config.API_HOST = ret.ip_address;
            config.API_PORT = ret.port;
        }).catch(err => {
            // Do Nothing
        })
    }

    userLogin (e) {
        this.props.authUserLogin(this.state.username, this.state.password);
        e.preventDefault();
    }

    render() {
        return (
            <ScrollView style={{padding: 20}}>
                <View style={{flex:1
                    }}>
                <FloatingLabelInput
                    id="username"
                    label='Username'
                    autoCapitalize='none'
                    autoFocus={true}
                    keyboardType='email-address'
                    value={this.state.username}
                    onChangeText={(text) => this.setState({ username: text })} />
                <FloatingLabelInput
                    id="password"
                    label='Password'
                    autoCapitalize='none'
                    secureTextEntry={true}
                    value={this.state.password}
                    onChangeText={(text) => this.setState({ password: text })} />
                </View>
                { renderIf(this.props.error) (
                    <View style={styles.error} >
                    <Text style={styles.errorMessage}>
                        { this.props.errorMessage }
                    </Text>
                    </View>
                )}
                <Spinner visible={this.props.isLoading} textContent={"Loading..."} textStyle={{color: '#FFF'}} />
                <View style={{margin: 7}}/>
                <Button onPress={(e) => this.userLogin(e)} title="Login"/>
            </ScrollView>
        );
    }
}


const mapStateToProps = (state: Object) => getAuthSelector(state);

const mapDispatchToProps = (dispatch: Function) => ({
    authUserLogin: (username,password) => dispatch(authUserLogin(username,password)),
});

Login.propTypes = {
    navigation: PropTypes.object.isRequired,
};

Login.navigationOptions = ({navigation})=> ({
    title: 'Log In',
    headerRight: (
        <TouchableHighlight
            onPress={() => navigation.navigate('Setting')}
            style={{margin: 10}}
        >
            <Icon
                name='cog'
                size={28}
                color='gray'
            />
        </TouchableHighlight>
    ),
    headerLeft: null, // Hide back button
});

export default connect(mapStateToProps, mapDispatchToProps)(Login);


import React, { Component } from 'react';
import { connect } from 'react-redux';
import { ScrollView, Text, View, Button, StyleSheet, TouchableHighlight, Alert } from 'react-native';
import { FloatingLabelInput } from '../components/FloatingLabelInput';
import { assetUpdate, assetEditable } from '../redux/actions/asset/asset';
import { getAssetSelector } from '../redux/reducers/asset';
import renderIf from 'render-if';
import Spinner from 'react-native-loading-spinner-overlay';
import Icon from 'react-native-vector-icons/FontAwesome';

const styles = StyleSheet.create({
    errorView: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    },
    errorMessage: {
        textAlign: 'center',
        margin: 3,
        fontWeight: 'bold',
        color: 'red'
    }
});

class AssetDetail extends Component {
    constructor (props) {
        super(props);
        this.state = {
            isEditable: false,
            id_data_pc: props.asset.id_data_pc,
            lantai_id: props.asset.lantai_id.toString(),
            map_no: props.asset.map_no,
            computer_name: props.asset.computer_name,
            ip: props.asset.ip,
            user_program: props.asset.user_program,
            processor: props.asset.processor,
            memory: props.asset.memory,
            hardisk: props.asset.hardisk,
            optical_drive: props.asset.optical_drive,
            cpu: props.asset.cpu,
            serial_number_cpu: props.asset.serial_number_cpu,
            barcode_cpu: props.asset.barcode_cpu,
            monitor: props.asset.monitor,
            serial_number_monitor: props.asset.serial_number_monitor,
            barcode_monitor: props.asset.barcode_monitor,
            keyboard: props.asset.keyboard,
            serial_number_keyboard: props.asset.serial_number_keyboard,
            barcode_keyboard: props.asset.barcode_keyboard,
            mouse: props.asset.mouse,
            serial_number_mouse: props.asset.serial_number_mouse,
            barcode_mouse: props.asset.barcode_mouse,
        };
    }

    componentWillMount() {
        const {setParams} = this.props.navigation;
        setParams({
            assetEditable: this.props.assetEditable
        });
    }

    mapStateToRequestPayload(asset) {
        return {
            idDataPc: asset.id_data_pc,
            lantaiId: asset.lantai_id,
            mapNo: asset.map_no,
            computerName: asset.computer_name,
            ipAddress: asset.ip,
            userProgram: asset.user_program,
            processor: asset.processor,
            memory: asset.memory,
            hardisk: asset.hardisk,
            opticalDrive: asset.optical_drive,
            cpu: asset.cpu,
            serialNumberCpu: asset.serial_number_cpu,
            barcodeCpu: asset.barcode_cpu,
            monitor: asset.monitor,
            serialNumberMonitor: asset.serial_number_monitor,
            barcodeMonitor: asset.barcode_monitor,
            keyboard: asset.keyboard,
            serialNumberKeyboard: asset.serial_number_keyboard,
            barcodeKeyboard: asset.barcode_keyboard,
            mouse: asset.mouse,
            serialNumberMouse: asset.serial_number_mouse,
            barcodeMouse: asset.barcode_mouse,
        }
    }

    updateAsset (e) {
        this.props.assetUpdate(this.mapStateToRequestPayload(this.state), this.props.userToken);
        e.preventDefault();
    }

    render() {
        return (
            <ScrollView
                contentContainerStyle={{ flexGrow: 1, flexDirection: 'column', justifyContent: 'space-between' }}
                style={{padding: 20}}>
                <FloatingLabelInput
                    label='Lantai'
                    autoCapitalize='none'
                    autoCorrect={false}
                    keyboardType='numeric'
                    editable={this.props.isEditable}
                    value={this.state.lantai_id}
                    onChangeText={(text) => this.setState({ lantai_id: text })} />
                <FloatingLabelInput
                    label='No. Map'
                    autoCapitalize='none'
                    autoCorrect={false}
                    editable={false}
                    value={this.state.map_no}
                    onChangeText={(text) => this.setState({ map_no: text })} />
                <FloatingLabelInput
                    label='Comp. Name'
                    autoCapitalize='none'
                    autoCorrect={false}
                    value={this.state.computer_name}
                    editable={this.props.isEditable}
                    onChangeText={(text) => this.setState({ computer_name: text })} />
                <FloatingLabelInput
                    label='IP Address'
                    autoCapitalize='none'
                    autoCorrect={false}
                    value={this.state.ip}
                    editable={this.props.isEditable}
                    onChangeText={(text) => this.setState({ ip: text })} />
                <FloatingLabelInput
                    label='User/Program'
                    autoCapitalize='none'
                    autoCorrect={false}
                    value={this.state.user_program}
                    editable={this.props.isEditable}
                    onChangeText={(text) => this.setState({ user_program: text })} />
                <FloatingLabelInput
                    label='Processor'
                    autoCapitalize='none'
                    autoCorrect={false}
                    value={this.state.processor}
                    editable={this.props.isEditable}
                    onChangeText={(text) => this.setState({ processor: text })} />
                <FloatingLabelInput
                    label='Memory'
                    autoCapitalize='none'
                    autoCorrect={false}
                    value={this.state.memory}
                    editable={this.props.isEditable}
                    onChangeText={(text) => this.setState({ memory: text })} />
                <FloatingLabelInput
                    label='HDD'
                    autoCapitalize='none'
                    autoCorrect={false}
                    value={this.state.hardisk}
                    editable={this.props.isEditable}
                    onChangeText={(text) => this.setState({ hardisk: text })} />
                <FloatingLabelInput
                    label='Optical Drive'
                    autoCapitalize='none'
                    autoCorrect={false}
                    value={this.state.optical_drive}
                    editable={this.props.isEditable}
                    onChangeText={(text) => this.setState({ optical_drive: text })} />
                <FloatingLabelInput
                    label='CPU'
                    autoCapitalize='none'
                    autoCorrect={false}
                    value={this.state.cpu}
                    onChangeText={(text) => this.setState({ cpu: text })} />
                <FloatingLabelInput
                    label='CPU S/N'
                    autoCapitalize='none'
                    autoCorrect={false}
                    value={this.state.serial_number_cpu}
                    editable={this.props.isEditable}
                    onChangeText={(text) => this.setState({ serial_number_cpu: text })} />
                <FloatingLabelInput
                    label='CPU B/C'
                    autoCapitalize='none'
                    autoCorrect={false}
                    value={this.state.barcode_cpu}
                    editable={this.props.isEditable}
                    onChangeText={(text) => this.setState({ barcode_cpu: text })} />
                <FloatingLabelInput
                    label='Monitor'
                    autoCapitalize='none'
                    autoCorrect={false}
                    value={this.state.monitor}
                    onChangeText={(text) => this.setState({ monitor: text })} />
                <FloatingLabelInput
                    label='Monitor S/N'
                    autoCapitalize='none'
                    autoCorrect={false}
                    value={this.state.serial_number_monitor}
                    editable={this.props.isEditable}
                    onChangeText={(text) => this.setState({ serial_number_monitor: text })} />
                <FloatingLabelInput
                    label='Monitor B/C'
                    autoCapitalize='none'
                    autoCorrect={false}
                    value={this.state.barcode_monitor}
                    editable={this.props.isEditable}
                    onChangeText={(text) => this.setState({ barcode_monitor: text })} />
                <FloatingLabelInput
                    label='Keyboard'
                    autoCapitalize='none'
                    autoCorrect={false}
                    value={this.state.keyboard}
                    editable={this.props.isEditable}
                    onChangeText={(text) => this.setState({ keyboard: text })} />
                <FloatingLabelInput
                    label='keyboard S/N'
                    autoCapitalize='none'
                    autoCorrect={false}
                    value={this.state.serial_number_keyboard}
                    editable={this.props.isEditable}
                    onChangeText={(text) => this.setState({ serial_number_keyboard: text })} />
                <FloatingLabelInput
                    label='Keyboard B/C'
                    autoCapitalize='none'
                    autoCorrect={false}
                    value={this.state.barcode_keyboard}
                    editable={this.props.isEditable}
                    onChangeText={(text) => this.setState({ barcode_keyboard: text })} />
                <FloatingLabelInput
                    label='Mouse'
                    autoCapitalize='none'
                    autoCorrect={false}
                    value={this.state.mouse}
                    editable={this.props.isEditable}
                    onChangeText={(text) => this.setState({ mouse: text })} />
                <FloatingLabelInput
                    label='Mouse S/N'
                    autoCapitalize='none'
                    autoCorrect={false}
                    value={this.state.serial_number_mouse}
                    editable={this.props.isEditable}
                    onChangeText={(text) => this.setState({ serial_number_mouse: text })} />
                <FloatingLabelInput
                    label='Mouse B/C'
                    autoCapitalize='none'
                    autoCorrect={false}
                    value={this.state.barcode_mouse}
                    editable={this.props.isEditable}
                    onChangeText={(text) => this.setState({ barcode_mouse: text })} />
                { renderIf(this.props.error) (
                    <View style={styles.error} >
                        <Text style={styles.errorMessage}>
                            { this.props.errorMessage }
                        </Text>
                    </View>
                )}
                <Spinner visible={this.props.isLoading} textContent={"Loading..."} textStyle={{color: '#FFF'}} />

                <View
                    style={{margin: 10, height: 90}}
                >
                    { renderIf(this.props.isEditable) (
                    <Button onPress={(e) => this.updateAsset(e)} title="Update"/>
                    )}
                </View>
            </ScrollView>
        );
    }
}

function showAlert(navigation) {
    Alert.alert(
        '',
        'Are you sure want to edit this asset?',
        [
            {text: 'Cancel', onPress: () => console.log('Cancel Pressed'), style: 'cancel'},
            {text: 'OK', onPress: function () {
                navigation.state.params.assetEditable(true);
            }},
        ],
        { cancelable: false }
    )
}


const mapStateToProps = (state: Object) => getAssetSelector(state);

const mapDispatchToProps = (dispatch: Function) => ({
    assetUpdate: (asset, userToken) => dispatch(assetUpdate(asset, userToken)),
    assetEditable: (isEditable) => dispatch(assetEditable(isEditable)),
});

AssetDetail.navigationOptions = ({navigation})=> ({
    title: 'Asset Detail',
    headerRight: (
        <TouchableHighlight
            onPress={() => showAlert(navigation)}
            style={{margin: 10}}
        >
            <Icon
                name='edit'
                size={28}
                color='blue'
            />
        </TouchableHighlight>
    ),
});

export default connect(mapStateToProps, mapDispatchToProps)(AssetDetail);
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { ScrollView, Text, TextInput, View, Button, StyleSheet } from 'react-native';
import { loadSetting, saveSetting } from '../redux/actions/setting/setting';
import { getSettingSelector } from '../redux/reducers/setting';
import renderIf from 'render-if';
import Spinner from 'react-native-loading-spinner-overlay';
import { config } from '../lib/config';
import { storage, storageKey } from '../utils/storage';

const styles = StyleSheet.create({
    errorView: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    },
    errorMessage: {
        textAlign: 'center',
        margin: 3,
        fontWeight: 'bold',
        color: 'red'
    }
});

class Setting extends Component {
    constructor (props) {
        super(props);
        this.state = {
            ip_address: props.ip_address,
            port: props.port,
            is_default_value: false
        };

    }

    componentWillReceiveProps(nextProps) {
        if (!nextProps.setting.is_default_value) {
            this.setState(nextProps.setting);
        }
    }

    componentWillMount() {
        storage.load({
            key: storageKey,

            // autoSync(default true) means if data not found or expired,
            // then invoke the corresponding sync method
            autoSync: true,

            // syncInBackground(default true) means if data expired,
            // return the outdated data first while invoke the sync method.
            // It can be set to false to always return data provided by sync method when expired.(Of course it's slower)
            syncInBackground: true,
        }).then(ret => {
            // found data go to then()
            ret.is_default_value = false;
            this.setState(ret);
        }).catch(err => {
            // any exception including data not found
            // goes to catch()
            switch (err.name) {
                case 'NotFoundError':
                    this.props.loadSetting();
                    break;
                case 'ExpiredError':
                    this.props.error = true;
                    this.props.errorMessage = err.message;
                    break;
            }
        })
    }

    updateSetting (e) {
        this.props.saveSetting(this.state);
        e.preventDefault();
    }

    resetDefault(e) {
        this.state = {
            ip_address: config.DEFAULT_API_HOST,
            port: config.DEFAULT_API_PORT,
        }
        this.props.saveSetting(this.state);
        e.preventDefault();
    }

    render() {
        return (
            <ScrollView style={{padding: 20}}>
                <TextInput
                    placeholder='IP Address Server'
                    autoCapitalize='none'
                    autoCorrect={false}
                    autoFocus={true}
                    keyboardType='numeric'
                    value={this.state.ip_address}
                    onChangeText={(text) => this.setState({ ip_address: text })} />
                <TextInput
                    placeholder='Port'
                    autoCapitalize='none'
                    autoCorrect={false}
                    keyboardType='numeric'
                    value={this.state.port}
                    onChangeText={(text) => this.setState({ port: text })} />
                { renderIf(this.props.error) (
                    <View style={styles.error} >
                        <Text style={styles.errorMessage}>
                            { this.props.errorMessage }
                        </Text>
                    </View>
                )}
                <Spinner visible={this.props.isLoading} textContent={"Loading..."} textStyle={{color: '#FFF'}} />
                <View style={{margin: 10, height: 50}}>
                    <Button
                        onPress={(e) => this.updateSetting(e)} title="Update"/>
                </View>
                <View style={{margin: 10, height: 50}}>
                    <Button
                        onPress={(e) => this.resetDefault(e)} title="Reset to default"/>
                </View>
            </ScrollView>
        );
    }
}

const mapStateToProps = (state: Object) => getSettingSelector(state);

const mapDispatchToProps = (dispatch: Function) => ({
    saveSetting: (setting) => dispatch(saveSetting(setting)),
    loadSetting: (setting) => dispatch(loadSetting(setting)),
});

Setting.navigationOptions = {
    title: 'Settings'
};

export default connect(mapStateToProps, mapDispatchToProps)(Setting);
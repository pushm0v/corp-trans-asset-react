import React, { Component } from 'react';
import { connect } from 'react-redux';
import { ScrollView, Text, View, Button, StyleSheet, TouchableHighlight, Alert } from 'react-native';
import {logoutUser, scanBarcodeByMapNo} from '../redux/actions/scan-barcode/scan-barcode';
import { getScanBarcodeSelector } from '../redux/reducers/scan-barcode';
import renderIf from 'render-if';
import QRCodeScanner from 'react-native-qrcode-scanner';
import Spinner from 'react-native-loading-spinner-overlay';
import Icon from 'react-native-vector-icons/FontAwesome';
import { storage, tokenKey } from '../utils/storage';
import { config } from '../lib/config';

const styles = StyleSheet.create({
    errorView: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    },
    errorMessage: {
        textAlign: 'center',
        margin: 3,
        fontWeight: 'bold',
        color: 'red'
    },
    barcode: {
        fontSize: 16,
        textAlign: 'center',
        fontWeight: 'bold',
        color: 'black'
    }
});

class ScanBarcode extends Component {
    constructor (props) {
        super(props);
        this.state = {
            map_no: ''
        };
    }

    componentWillMount() {
        const {setParams} = this.props.navigation;
        setParams({
            logoutUser: this.props.logoutUser
        });
        storage.load({
            key: tokenKey,
            autoSync: true,
            syncInBackground: true,
        }).then(ret => {
            config.TOKEN = ret.userToken;
        }).catch(err => {
            // any exception including data not found
            // goes to catch()
            switch (err.name) {
                case 'NotFoundError':
                    console.log(err);
                    break;
                case 'ExpiredError':
                    console.log(err);
                    break;
            }
        })
    }

    scanBarcode (e) {
        this.setState({map_no: e.data});
        this.props.scanBarcodeByMapNo(this.state.map_no);
    }

    render() {
        let scanner;

        const startScan = () => {
            if (scanner) {
                scanner._setScanning(false);
            }
        };

        return (
            <ScrollView>
                <View style={{flex:1}}>
                    <QRCodeScanner
                        reactivate={false}
                        ref={(camera) => scanner = camera}
                        showMarker={true}
                        checkAndroid6Permissions={true}
                        onRead={(e) => this.scanBarcode(e)}
                        bottomContent={(<Button title="Rescan" onPress={() => startScan()} />)}
                    />
                </View>
                <View style={{marginTop: 5}} />
                <View style={{flex:1
                }}>
                    <Text
                        autoCapitalize='none'
                        autoFocus={false}
                        editable={false}
                        keyboardType='default'
                        value={this.state.map_no}
                        style={styles.barcode}
                        onChangeText={(text) => this.setState({ map_no: text })} />
                </View>
                { renderIf(this.props.error) (
                    <View style={styles.error} >
                        <Text style={styles.errorMessage}>
                            { this.props.errorMessage }
                        </Text>
                    </View>
                )}
                <View style={{margin: 7}}/>
                <Spinner visible={this.props.isLoading} textContent={"Loading..."} textStyle={{color: '#FFF'}} />
            </ScrollView>
        );
    }
}

function showAlert(navigation) {
    Alert.alert(
        '',
        'Are you sure want to logout?',
        [
            {text: 'Cancel', onPress: () => console.log('Cancel Pressed'), style: 'cancel'},
            {text: 'OK', onPress: function () {
                storage.clearMapForKey(tokenKey);
                navigation.state.params.logoutUser();
            }},
        ],
        { cancelable: false }
    )
}

const mapStateToProps = (state: Object) => getScanBarcodeSelector(state);

const mapDispatchToProps = (dispatch: Function) => ({
    scanBarcodeByMapNo: (mapNo) => dispatch(scanBarcodeByMapNo(mapNo)),
    logoutUser: () => dispatch(logoutUser()),
});

ScanBarcode.navigationOptions = ({navigation})=> ({
    title: 'Scan Barcode',
    headerRight: (
        <TouchableHighlight
            onPress={() => showAlert(navigation)}
            style={{margin: 10}}
        >
            <Icon
                name='sign-out'
                size={28}
                color='red'
            />
        </TouchableHighlight>
    ),
    headerLeft: null, // Hide back button
});

export default connect(mapStateToProps, mapDispatchToProps)(ScanBarcode);
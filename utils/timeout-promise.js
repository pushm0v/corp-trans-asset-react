export function timeoutPromise(ms, promise) {
    return new Promise(function(resolve, reject) {
        setTimeout(function() {
            reject(new Error("Request timeout"))
        }, ms)
        promise.then(resolve, reject)
    })
}
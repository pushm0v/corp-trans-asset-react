export const config = {
    APP_TITLE: 'TransTV Asset Management',
    DEFAULT_API_HOST: '192.168.2.124',
    DEFAULT_API_PORT: '4567',
    API_HOST: '192.168.2.124',
    API_PORT: '4567',
    DEFAULT_TIMEOUT: 10000,
    TOKEN: ''
};

export function getAPIUrl() {
    return "http://" + config.API_HOST + ":" + config.API_PORT + "/api/v1";
};

export function getToken() {
    return config.TOKEN;
}
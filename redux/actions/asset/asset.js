import { assetFailed } from './asset-error';
import { assetRequest } from './asset-request';
import { assetSuccess } from './asset-success';
import { assetEdit } from './asset-edit';
import { config, getAPIUrl, getToken } from '../../../lib/config';
import { timeoutPromise } from '../../../utils/timeout-promise';

export const assetUpdate = (asset, userToken) => (
    (dispatch: Function) => {
        dispatch(assetRequest(asset));
        timeoutPromise(config.DEFAULT_TIMEOUT, fetch(getAPIUrl() + '/asset',
            {
                method: "POST",
                headers: {
                    "User-Token": getToken()
                },
                body: JSON.stringify(asset)
            }))
        .then(response =>
            response.json().then(json => ({
                    status: response.status,
                    json
                })
            ))
        .then(({ status, json }) => {
                if (status >= 400) {
                    // Status looks bad
                    dispatch(assetFailed(json))
                } else {
                    // Status looks good
                    dispatch(assetSuccess(json.data))
                }
        })
        .catch((err) => {
            dispatch(assetFailed({error: {message: err.message}}))
        });
}
);

export const assetEditable = (isEditable) => (
    (dispatch: Function) => {
        dispatch(assetEdit(isEditable));
    }
);
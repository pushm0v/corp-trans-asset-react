import { ASSET_EDIT } from '../../../constants/ActionTypes';

export const assetEdit = (isEditable) => (
    {
        type: ASSET_EDIT,
        payload: {
            isLoggedIn: true,
            isEditable: isEditable,
            isLoading: false,
            message: ''
        },
    }
);
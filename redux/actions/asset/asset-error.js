import { ASSET_FAILED } from '../../../constants/ActionTypes';

export const assetFailed = (data) => (
    {
        type: ASSET_FAILED,
        payload: {
            isLoading: true,
            isEditable: true,
            error: true,
            message: data.error.message
        },
    }
);

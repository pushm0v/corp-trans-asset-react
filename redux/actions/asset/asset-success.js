import { ASSET_SUCCESS } from '../../../constants/ActionTypes';

export const assetSuccess = (data: Object) => (
    {
        type: ASSET_SUCCESS,
        payload: {
            isLoggedIn: true,
            isEditable: false,
            isLoading: false,
            message: data.message
        },
    }
);
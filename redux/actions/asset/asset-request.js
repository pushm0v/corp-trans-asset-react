import { ASSET_REQUEST } from '../../../constants/ActionTypes';

export const assetRequest = (data: Object) => (
    {
        type: ASSET_REQUEST,
        payload: {
            isLoading: true,
            isEditable: true,
            id_data_pc: data.id_data_pc,
            lantai_id: data.lantai_id,
            map_no: data.map_no,
            computer_name: data.computer_name,
            ip: data.ip,
            user_program: data.user_program,
            processor: data.processor,
            memory: data.memory,
            hardisk: data.hardisk,
            optical_drive: data.optical_drive,
            cpu: data.cpu,
            serial_number_cpu: data.serial_number_cpu,
            barcode_cpu: data.barcode_cpu,
            monitor: data.monitor,
            serial_number_monitor: data.serial_number_monitor,
            barcode_monitor: data.barcode_monitor,
            keyboard: data.keyboard,
            serial_number_keyboard: data.serial_number_keyboard,
            barcode_keyboard: data.barcode_keyboard,
            mouse: data.mouse,
            serial_number_mouse: data.serial_number_mouse,
            barcode_mouse: data.barcode_mouse,
        },
    }
);

import { AUTH_USER_LOGIN_REQUEST } from '../../../constants/ActionTypes';

export const userLoginRequest = (username, password) => (
    {
        type: AUTH_USER_LOGIN_REQUEST,
        payload: {
            isLoading: true,
            username: username,
            password: password,
        },
    }
);

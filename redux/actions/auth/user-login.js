import { userLoginFailed } from './user-login-error';
import { userLoginRequest } from './user-login-request';
import { userLoginSuccess } from './user-login-success';
import { userLogoutSuccess } from './user-logout-success';
import { config, getAPIUrl } from "../../../lib/config";
import { timeoutPromise } from '../../../utils/timeout-promise';

import { storage } from '../../../utils/storage';
let tokenKey = 'token';

export const authUserLogin = (username, password) => (
    (dispatch: Function) => {
        dispatch(userLoginRequest(username, password));
        timeoutPromise(config.DEFAULT_TIMEOUT, fetch(getAPIUrl() + '/login',
            {
                method: "POST",
                body: JSON.stringify({
                    username: username,
                    password: password
                })
            }))
            .then(response =>
                response.json().then(json => ({
                        status: response.status,
                        json
                    })
                ))
            .then(({ status, json }) => {
                    if (status >= 400) {
                        // Status looks bad
                        dispatch(userLoginFailed(json))
                    } else {
                        config.TOKEN = json.data.userToken;
                        storage.save({
                            key: tokenKey,
                            data: json.data,
                            expires: null
                        });
                        dispatch(userLoginSuccess(json.data))
                    }
                })
            .catch((err) => {
                dispatch(userLoginFailed({error: {message: err.message}}))
            });
    }
);

export const authUserLogout = () => (
    (dispatch: Function) => {
        dispatch(userLogoutSuccess());
    }
);
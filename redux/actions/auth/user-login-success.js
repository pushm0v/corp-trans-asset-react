import { AUTH_USER_LOGIN_SUCCESS } from '../../../constants/ActionTypes';

export const userLoginSuccess = (data: Object) => (
    {
        type: AUTH_USER_LOGIN_SUCCESS,
        payload: {
            isLoggedIn: true,
            message: data.message,
            userToken: data.userToken,
            username: data.username,
        },
    }
);
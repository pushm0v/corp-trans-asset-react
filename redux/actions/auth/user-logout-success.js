import { AUTH_USER_LOGOUT_SUCCESS } from '../../../constants/ActionTypes';

export const userLogoutSuccess = () => (
    {
        type: AUTH_USER_LOGOUT_SUCCESS,
        payload: {
            isLoggedIn: false,
            error: false,
            userToken: '',
            username: '',
        },
    }
);

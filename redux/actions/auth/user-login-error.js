import { AUTH_USER_LOGIN_FAILED } from '../../../constants/ActionTypes';

export const userLoginFailed = (data) => (
    {
        type: AUTH_USER_LOGIN_FAILED,
        payload: {
            isLoggedIn: false,
            error: true,
            message: data.error.message
        },
    }
);

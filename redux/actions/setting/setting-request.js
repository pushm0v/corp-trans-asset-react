import { SETTING_REQUEST } from '../../../constants/ActionTypes';

export const settingRequest = (setting) => (
    {
        type: SETTING_REQUEST,
        payload: {
            isLoading: false,
            error: false,
            ip_address: setting.ip_address,
            port: setting.port
        },
    }
);

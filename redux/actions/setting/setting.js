import { settingRequest } from './setting-request';
import { settingSuccess } from './setting-success';
import { settingLoad } from './setting-load';

import { storage } from '../../../utils/storage';
import { config } from '../../../lib/config';

let storageKey = 'applicationServer';

export const saveSetting = (setting) => (
    (dispatch: Function) => {
        dispatch(settingRequest(setting))
        storage.save({
            key: storageKey,   // Note: Do not use underscore("_") in key!
            data: setting,

            // if not specified, the defaultExpires will be applied instead.
            // if set to null, then it will never expire.
            expires: null
        });
        config.API_HOST = setting.ip_address;
        config.API_PORT = setting.port;
        dispatch(settingSuccess(setting));
    }
);

export const loadSetting = (setting) => (
    (dispatch: Function) => {
        config.API_HOST = config.DEFAULT_API_HOST;
        config.API_PORT = config.DEFAULT_API_PORT;
        var defaultValue = true;
        if (setting != undefined) {
            config.API_HOST = setting.ip_address;
            config.API_PORT = setting.port;
            defaultValue = false;
        }
        let setting = {
            ip_address: config.API_HOST,
            port: config.API_PORT,
            is_default_value: defaultValue
        }
        dispatch(settingLoad(setting));
    }
);

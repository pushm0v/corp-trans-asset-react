import { SETTING_LOAD } from '../../../constants/ActionTypes';

export const settingLoad = (data: Object) => (
    {
        type: SETTING_LOAD,
        payload: {
            isLoggedIn: true,
            isLoading: false,
            is_default_value: true,
            message: data.message,
            ip_address: data.ip_address,
            port: data.port,
        },
    }
);

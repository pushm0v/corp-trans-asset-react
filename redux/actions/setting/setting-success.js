import { SETTING_SUCCESS } from '../../../constants/ActionTypes';

export const settingSuccess = (data: Object) => (
    {
        type: SETTING_SUCCESS,
        payload: {
            isLoggedIn: true,
            isLoading: false,
            message: data.message,
            ip_address: data.ip_address,
            port: data.port,
        },
    }
);
import { SETTING_FAILED } from '../../../constants/ActionTypes';

export const settingFailed = (data) => (
    {
        type: SETTING_FAILED,
        payload: {
            isLoading: true,
            error: true,
            message: data.message
        },
    }
);

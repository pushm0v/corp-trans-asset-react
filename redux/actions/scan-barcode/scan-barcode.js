import { scanBarcodeFailed } from './scan-barcode-error';
import { scanBarcodeRequest } from './scan-barcode-request';
import { scanBarcodeSuccess } from './scan-barcode-success';
import { authUserLogout } from '../auth/user-login';
import { config, getAPIUrl, getToken } from '../../../lib/config';
import { timeoutPromise } from '../../../utils/timeout-promise';

export const scanBarcodeByMapNo = (mapNo, userToken) => (
    (dispatch: Function) => {
        dispatch(scanBarcodeRequest(mapNo));
        timeoutPromise(config.DEFAULT_TIMEOUT, fetch(getAPIUrl() + '/asset?map_no=' + mapNo,
            {
                method: "GET",
                headers: {
                    "User-Token": getToken()
                }
            }))
        .then(response =>
            response.json().then(json => ({
                    status: response.status,
                    json
                })
            ))
        .then(({ status, json }) => {
                if (status >= 400) {
                    // Status looks bad
                    dispatch(scanBarcodeFailed(json))
                } else {
                    // Status looks good
                    dispatch(scanBarcodeSuccess(json.data))
                }
        })
        .catch((err) => dispatch(scanBarcodeFailed({error : {message: err.message}})));
    }
);

export const logoutUser = () => (
    (dispatch: Function) => {
        dispatch(authUserLogout());
    }
);
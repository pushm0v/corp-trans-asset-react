import { SCAN_BARCODE_SUCCESS } from '../../../constants/ActionTypes';

export const scanBarcodeSuccess = (data: Object) => (
    {
        type: SCAN_BARCODE_SUCCESS,
        data: {
            isLoggedIn: true,
            message: data.message,
            id_data_pc: data.idDataPc,
            lantai_id: data.lantaiId,
            map_no: data.mapNo,
            computer_name: data.computerName,
            ip: data.ipAddress,
            user_program: data.userProgram,
            processor: data.processor,
            memory: data.memory,
            hardisk: data.hardisk,
            optical_drive: data.opticalDrive,
            cpu: data.cpu,
            serial_number_cpu: data.serialNumberCpu,
            barcode_cpu: data.barcodeCpu,
            monitor: data.monitor,
            serial_number_monitor: data.serialNumberMonitor,
            barcode_monitor: data.barcodeMonitor,
            keyboard: data.keyboard,
            serial_number_keyboard: data.serialNumberKeyboard,
            barcode_keyboard: data.barcodeKeyboard,
            mouse: data.mouse,
            serial_number_mouse: data.serialNumberMouse,
            barcode_mouse: data.barcodeMouse,
        },
    }
);
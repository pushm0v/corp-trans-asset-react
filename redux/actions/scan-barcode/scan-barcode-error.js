import { SCAN_BARCODE_FAILED } from '../../../constants/ActionTypes';

export const scanBarcodeFailed = (data: Object) => (
    {
        type: SCAN_BARCODE_FAILED,
        payload: {
            isLoading: false,
            error: true,
            message: data.error.message
        },
    }
);

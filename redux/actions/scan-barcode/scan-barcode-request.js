import { SCAN_BARCODE_REQUEST } from '../../../constants/ActionTypes';

export const scanBarcodeRequest = (mapNo) => (
    {
        type: SCAN_BARCODE_REQUEST,
        payload: {
            isLoading: true,
            map_no: mapNo
        },
    }
);

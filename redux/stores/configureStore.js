import {createStore, applyMiddleware} from 'redux';
import rootReducer from '../reducers';
import thunk from 'redux-thunk';
import loggingMiddleware from './middleware/logging';
import { navigationMiddleware } from "../../utils/redux";

export default function configureStore(initialState: Object) {
    return createStore(
        rootReducer,
        initialState,
        applyMiddleware(thunk, loggingMiddleware, navigationMiddleware)
    );
}
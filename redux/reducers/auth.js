import {
    AUTH_USER_LOGIN_FAILED,
    AUTH_USER_LOGIN_REQUEST,
    AUTH_USER_LOGIN_SUCCESS,
    AUTH_USER_LOGOUT_SUCCESS,
} from '../../constants/ActionTypes';

const initialState = {
    data: {},
    isLoading: false,
    error: false
};

export const getAuthSelector = (state: Object) => ({
    ...state.data,
    isLoading: state.userAuth.isLoading,
    error: state.userAuth.error,
    errorMessage: state.userAuth.message
});

export default function authReducer(state = initialState, action) {
    switch (action.type) {
        case AUTH_USER_LOGIN_SUCCESS: {
            return {
                isLoading: false,
                error: false,
                data: {
                    message: action.payload.message,
                    userToken: action.payload.userToken,
                    username: action.payload.username,
                }
            };
        }
        case AUTH_USER_LOGIN_REQUEST: {
            return {
                isLoading: true,
                error: false,
                data: {
                    username: action.payload.username,
                    password: action.payload.password,
                },
            };
        }
        case AUTH_USER_LOGIN_FAILED: {
            return {
                ...state,
                isLoading: false,
                error: true,
                message: action.payload.message
            };
        }
        case AUTH_USER_LOGOUT_SUCCESS: {
            return {
                isLoading: false,
                error: false,
            };
        }
        default: {
            return state;
        }
    }
}
import {
    ASSET_FAILED,
    ASSET_REQUEST,
    ASSET_SUCCESS,
    ASSET_EDIT,
} from '../../constants/ActionTypes';

const initialState = {
    data: {},
    isLoading: false,
    isLoggedIn: true,
    error: false,
};

export const getAssetSelector = (state: Object) => ({
    ...state.data,
    isEditable: state.asset.isEditable,
    isLoading: state.asset.isLoading,
    asset: state.scanBarcode.data,
    error: state.asset.error,
    errorMessage: state.asset.message
});

export default function assetReducer(state = initialState, action) {
    switch (action.type) {
        case ASSET_SUCCESS: {
            return {
                isLoading: false,
                isEditable: false,
                error: false,
                isLoggedIn: true,
                message: action.payload.message
            };
        }
        case ASSET_EDIT: {
            return {
                isLoading: false,
                isEditable: true,
                error: false,
                isLoggedIn: true,
                message: action.payload.message
            };
        }
        case ASSET_REQUEST: {
            return {
                isLoading: true,
                error: false,
                data: {
                    map_no: action.payload.map_no,
                },
            };
        }
        case ASSET_FAILED: {
            return {
                ...state,
                isLoading: false,
                isLoggedIn: false,
                error: true,
                message: action.payload.message
            };
        }
        default: {
            return state;
        }
    }
}
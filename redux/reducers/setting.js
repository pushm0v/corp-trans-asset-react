import {
    SETTING_REQUEST,
    SETTING_SUCCESS,
    SETTING_FAILED,
    SETTING_LOAD
} from '../../constants/ActionTypes';

import { config } from '../../lib/config';

const initialState = {
    data: {},
    isLoading: false,
    error: false,
    is_default_value: true,
    ip_address: '',
    port: '',
};

export const getSettingSelector = (state: Object) => ({
    ...state,
    isLoading: state.setting.isLoading,
    error: state.setting.error,
    errorMessage: state.setting.message,
    is_default_value: state.setting.is_default_value,
    ip_address: state.setting.ip_address, //(state.setting.ip_address == undefined) ? config.DEFAULT_API_HOST : state.setting.ip_address,
    port: state.setting.port, //(state.setting.port == undefined) ? config.DEFAULT_API_PORT : state.setting.port,
});

export default function settingReducer(state = initialState, action) {
    switch (action.type) {
        case SETTING_SUCCESS: {
            return {
                isLoading: false,
                error: false,
                message: action.payload.message,
                ip_address: action.payload.ip_address,
                port: action.payload.port,
                is_default_value: action.payload.is_default_value,
            };
        }
        case SETTING_LOAD: {
            return {
                isLoading: false,
                error: false,
                message: action.payload.message,
                ip_address: action.payload.ip_address,
                port: action.payload.port,
                is_default_value: action.payload.is_default_value,
            };
        }
        case SETTING_REQUEST: {
            return {
                isLoading: false,
                error: false,
                ip_address: action.payload.ip_address,
                port: action.payload.port,
            };
        }
        case SETTING_FAILED: {
            return {
                ...state,
                isLoading: false,
                error: true,
                message: action.payload.message
            };
        }
        default: {
            return state;
        }
    }
}
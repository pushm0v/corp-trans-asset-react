import {
    SCAN_BARCODE_FAILED,
    SCAN_BARCODE_REQUEST,
    SCAN_BARCODE_SUCCESS,
} from '../../constants/ActionTypes';

const initialState = {
    data: {},
    isLoading: false,
    hasData: false,
    error: false,
};

export const getScanBarcodeSelector = (state: Object) => ({
    ...state.data,
    isLoading: state.scanBarcode.isLoading,
    error: state.scanBarcode.error,
    errorMessage: state.scanBarcode.message
});

export default function scanBarcodeReducer(state = initialState, action) {
    switch (action.type) {
        case SCAN_BARCODE_SUCCESS: {
            return {
                isLoading: false,
                error: false,
                hasData: true,
                data: {
                    id_data_pc: action.data.id_data_pc,
                    lantai_id: action.data.lantai_id,
                    map_no: action.data.map_no,
                    computer_name: action.data.computer_name,
                    ip: action.data.ip,
                    user_program: action.data.user_program,
                    processor: action.data.processor,
                    memory: action.data.memory,
                    hardisk: action.data.hardisk,
                    optical_drive: action.data.optical_drive,
                    cpu: action.data.cpu,
                    serial_number_cpu: action.data.serial_number_cpu,
                    barcode_cpu: action.data.barcode_cpu,
                    monitor: action.data.monitor,
                    serial_number_monitor: action.data.serial_number_monitor,
                    barcode_monitor: action.data.barcode_monitor,
                    keyboard: action.data.keyboard,
                    serial_number_keyboard: action.data.serial_number_keyboard,
                    barcode_keyboard: action.data.barcode_keyboard,
                    mouse: action.data.mouse,
                    serial_number_mouse: action.data.serial_number_mouse,
                    barcode_mouse: action.data.barcode_mouse,
                }
            };
        }
        case SCAN_BARCODE_REQUEST: {
            return {
                isLoading: true,
                error: false,
                hasData: false,
                data: {
                    map_no: action.payload.map_no,
                },
            };
        }
        case SCAN_BARCODE_FAILED: {
            return {
                ...state,
                isLoading: false,
                error: true,
                message: action.payload.message
            };
        }
        default: {
            return state;
        }
    }
}
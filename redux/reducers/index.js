import { combineReducers } from 'redux';
import { NavigationActions } from 'react-navigation';
import { AppNavigator } from '../../navigators/AppNavigator';
import userAuth from './auth';
import asset from './asset';
import scanBarcode from './scan-barcode';
import setting from './setting';

import {
    AUTH_USER_LOGIN_SUCCESS,
    AUTH_USER_LOGOUT_SUCCESS,
    SCAN_BARCODE_SUCCESS,
    ASSET_SUCCESS,
    SETTING_SUCCESS,
    SETTING_OPEN,
} from '../../constants/ActionTypes';

const firstAction = AppNavigator.router.getActionForPathAndParams('ScanBarcode');
const tempNavState = AppNavigator.router.getStateForAction(firstAction);
const secondAction = AppNavigator.router.getActionForPathAndParams('Login');
const initialNavState = AppNavigator.router.getStateForAction(
    secondAction,
    tempNavState
);

function nav(state = initialNavState, action) {
    let nextState;
    switch (action.type) {
        case 'Login':
        case AUTH_USER_LOGIN_SUCCESS:
            nextState = AppNavigator.router.getStateForAction(
                NavigationActions.back(),
                state
            );
            break;
        case 'Logout':
        case AUTH_USER_LOGOUT_SUCCESS:
            nextState = AppNavigator.router.getStateForAction(
                NavigationActions.navigate({ routeName: 'Login' }),
                state
            );
            break;
        case SCAN_BARCODE_SUCCESS:
            nextState = AppNavigator.router.getStateForAction(
                NavigationActions.navigate({ routeName: 'AssetDetail' }),
                state
            );
            break;
        case ASSET_SUCCESS:
            nextState = AppNavigator.router.getStateForAction(
                NavigationActions.navigate({ routeName: 'ScanBarcode' }),
                state
            );
            break;
        case SETTING_SUCCESS:
            nextState = AppNavigator.router.getStateForAction(
                NavigationActions.back(),
                state
            );
            break;
        case SETTING_OPEN:
            nextState = AppNavigator.router.getStateForAction(
                NavigationActions.navigate({ routeName: 'Setting' }),
                state
            );
            break;
        default:
            nextState = AppNavigator.router.getStateForAction(action, state);
            break;
    }

    // Simply return the original `state` if `nextState` is null or undefined.
    return nextState || state;
}

const initialAuthState = { isLoggedIn: false };

function auth(state = initialAuthState, action) {
    switch (action.type) {
        case 'Login':
        case AUTH_USER_LOGIN_SUCCESS:
            return { ...state, isLoggedIn: true };
        case 'Logout':
        case AUTH_USER_LOGOUT_SUCCESS:
            return { ...state, isLoggedIn: false };
        default:
            return state;
    }
}

const rootReducer = combineReducers({
    nav,
    auth,
    userAuth,
    asset,
    scanBarcode,
    setting,
});

export default rootReducer;
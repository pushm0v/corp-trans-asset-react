import React from 'react';
import { AppRegistry } from 'react-native';
import { Provider } from 'react-redux';
import configureStore from './redux/stores/configureStore';
import AppWithNavigationState from './navigators/AppNavigator';

const store = configureStore({});

class TransCorp extends React.Component {
  render() {
    return (
        <Provider store={store}>
            <AppWithNavigationState />
        </Provider>
    );
  }
}
AppRegistry.registerComponent('TransCorp', () => TransCorp);

export default TransCorp;



